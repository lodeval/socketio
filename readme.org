* Socket.io

** Installation

#+BEGIN_SRC shell
npm install
#+END_SRC


** Code de base du serveur et premier démarrage

Il faut récupérer une référence au serveur http, et la donner à la fonction
d'initialisation de ~socket.io~:


#+BEGIN_SRC javascript
  const http = require('http')
  const fs = require('fs')

  const server = http.createServer((req, res) => {
    fs.readFile('./index.html', 'utf-8', function(error, content) {
      res.writeHead(200, {"Content-Type": "text/html"})
      res.end(content)
    })
  })

  // socket.io
  const io = require('socket.io').listen(server)

  // basic usage
  io.sockets.on('connection', (socket) => {
      console.log('client connected')
  })

  server.listen(process.env.PORT || 3000)
#+END_SRC

Si on utilise express, il faut adapter légèrement:

#+BEGIN_SRC javascript :tangle server.js
  const express = require('express')
  const app = express()
  const http = require('http').Server(app)
  const io = require('socket.io')(http);

  app.get('/', (req, res) => res.sendFile('./index.html', {root: __dirname}))

  // basic usage
  io.sockets.on('connection', (socket) => {
    console.log(`client connected: ${socket.id}`)
  })

  http.listen(process.env.PORT || 3000)
#+END_SRC

Dans le fichier html, on charge le code client avec la route
~/socket.io/socket.io.js~ positionnée automatiquement par ~socket.io~

#+BEGIN_SRC html :tangle index.html
  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8" />
      <title>Socket.io</title>
    </head>
    <body>
      <h1>socket.io !</h1>
      <script src="/socket.io/socket.io.js"></script>
      <script>
        const socket = io()
      </script>
    </body>
  </html>
#+END_SRC

Tester le code avec 

#+BEGIN_SRC shell
npm start
#+END_SRC

Rendez vous sur [[http://localhost:3000]] avec votre navigateur.
Si tout se passe bien, sur la console va s'afficher un message de la forme :

#+BEGIN_EXAMPLE
client connected: XYYYYYYYYYZZZZZ
#+END_EXAMPLE

** Explorer l'API socket.io à la main

Démarrez le serveur avec ~npm start~.

Nous allons explorer les fonctionnalités une par une.
#+begin_src javascript 
io.on('connection', (socket) => {
  // each socket is associated to a specific client
  console.log(`client connected: ${socket.id}`)

  // triggers when a client connects
  io.clients((err, clients) => {
    // broadcasts to all connected clients
    io.emit('clients', clients)
  })

  // when on a client : socket.send('stuff') or socket.emit('message', 'stuff')
  socket.on('message', console.log)
  socket.on('other', console.log)

  // broadcasts to all client BUT this one
  socket.on('from_one_to_other', data => {
    socket.broadcast.emit('broadcast', `sent from ${socket.id}: ${data}`)
  })


  // room : limit broadcast space
  socket.on('join', room => socket.join(room))
  socket.on('leave', room => socket.leave(room))
  socket.on('broadcast', (room, msg) => socket.to(room).send(msg))

})
#+end_src

*** Notion de socket
- socket :: un canal de communication entre le serveur et un onglet d'un
  navigateur. Chaque onglet d'un client web est repéré par un identifiant unique.

#+begin_src javascript
io.on('connection', (socket) => {
  // each socket is associated to a specific client
  console.log(`client connected: ${socket.id}`)
}
#+end_src

Ouvrez une première fenêtre sur http://localhost:3000

Ouvrez la console. Grâce au code dans [[file:index.html][index.html]], à chaque fois qu'un client se
connecte, la liste des idenfiants de tous les clients est diffusée à tous les
clients :
#+begin_src javascript
socket.on('connect', () => {
  socket.on('clients', list => {
    console.log(list)
  })
}
#+end_src

Dans la console, utilisez `socket.id` pour repérer l'identifiant de votre
client, puis personnalisez quelque peu le message affiché quand un nouveau
client se connecte. Pour tester, ouvrez un autre onglet sur
http://localhost:3000

#+begin_src javascript
socket.on('clients', ids => {
  ids.map(id => id == socket.id ? "it's a me, Marioooo" : `Is it you, Luigi, hiding behind this id = ${id} ?`)
     .forEach(console.log)
})
#+end_src

*** API Websocket basique

Socket.io est une surcouche des websockets, on retrouve donc:

#+begin_src javascript
  // when on a client : socket.send('stuff') or socket.emit('message', 'stuff')
  socket.on('message', console.log)
  socket.on('other', console.log)
#+end_src

Dans la console de votre client, testez donc l'envoi de message au serveur, qui
va les afficher sur sa console dans le terminal.


#+begin_src javascript
socket.send(`it's a meeee, Mario ! id = ${socket.id}`) 
#+end_src

Socket.io permet de personnaliser le nom de l'événement :

#+begin_src javascript
socket.emit('other', `Or am I Luigi ? id = ${socket.id}`) 
#+end_src

*** Diffuser à tous les clients connectés
Seul le serveur connaît tous les clients, alors qu'un client particulier ne
connaît que le serveur.

La notion de diffusion permet au serveur d'envoyer un message à tous les clients
connectés:

#+begin_src javascript
// server side
io.emit('flood', 'I see you all')
#+end_src

Un cas fréquent est de vouloir diffuser le message provenant d'un client à tous
les autres, /sans/ le retransmettre au client initial. C'est ce que fait le code
serveur suivant :

#+begin_src javascript
  // broadcasts to all client BUT this one
  socket.on('from_one_to_other', data => {
    socket.broadcast.emit('broadcast', `sent from ${socket.id}: ${data}`)
  })
#+end_src

Pour tester cette fonctionnalité, il vous faut au moins deux onglets ouverts.
Dans l'un d'eux :
#+begin_src javascript
socket.emit('from_one_to_other', 'I speak to everybody')
#+end_src

Mais ??? Rien ne se passe dans l'autre ? C'est normal, nous ne nous sommes pas
mis en écoute dans l'autre onglet. Corrigez donc cela :
#+begin_src javascript
socket.on('broadcast', console.log)
#+end_src

Relancez l'émission du message dans le premier onglet, cela marche comme prévu
maintenant.

À noter que l'on peut se désabonner à tous moment:
#+begin_src javascript
socket.removeEventListener('broadcast')
#+end_src

*** Limiter l'espace de diffusion
Pour qu'un sous ensemble des clients puisse se parler entre eux par diffusion,
on introduit la notion de /canal de diffusion/, ou /room/ dans socket.io :

#+begin_src javascript
  // room : limit broadcast space
  socket.on('join', room => socket.join(room))
  socket.on('leave', room => socket.leave(room))
  socket.on('broadcast', (room, msg) => socket.to(room).send(msg))
#+end_src

Rappelez-vous: seul le serveur connaît tous les clients. Donc c'est au niveau du
serveur que se gère l'abonnement aux différents canaux de diffusion.

Pour tester le code existant, dans un premier onglet, on affiche tous les
messages reçu sur l'événement par défaut /message/ :
#+begin_src javascript
socket.on('message', console.log)
#+end_src

Dans un deuxième onglet :
#+begin_src javascript
// join a room
socket.emit('join', 'testroom')
// send a mesg to testroom
socket.emit('broadcast', 'testroom', 'I feel alone')
#+end_src

Le message n'est pas diffusé au premier onglet ! Rejoignons maintenant le même
canal dans le premier onglet:
#+begin_src javascript
socket.emit('join', 'testroom')
#+end_src

Re émettez le message dans le 2e onglet, il est bien reçu par le premier à
présent.

Testez le désabonnement au canal :
#+begin_src javascript
socket.emit('leave', 'testscript') 
#+end_src
